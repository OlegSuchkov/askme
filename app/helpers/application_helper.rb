module ApplicationHelper
  def user_avatar(user)
    if user.avatar_url.present?
      user.avatar_url
    else
      asset_path 'avatar.jpg'
    end
  end

  def sklonenie(number, var1, var2, var3)
    remainder = number % 10
    return var3 if number.to_s.match?(/(11|12|13|14)$/)
    return var1 if remainder == 1
    return var2 if (remainder >= 2) && (remainder <= 4)
    return var3 if (remainder > 4) || remainder.zero?
  end
end
