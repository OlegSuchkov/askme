class UsersController < ApplicationController
  def index
    @users = [
      User.new(
        id: 1,
        name: 'Josh',
        username: 'solar',
        avatar_url: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=100'
      ),
      User.new(
        id: 2,
        name: 'Nick',
        username: 'bear_brown'
      )
    ]
  end

  def new
  end

  def edit
  end

  def show
    @user = User.new(
      name: 'Josh',
      username: 'solar',
      avatar_url: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=100'
    )

    @questions = [
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2017')),
      Question.new(text: 'Where are you?', created_at: Date.parse('31.10.2017'))
    ]

    @new_question = Question.new
  end
end
